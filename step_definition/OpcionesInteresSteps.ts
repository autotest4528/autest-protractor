import { config } from "../config/config";
import { assert, Assertion } from 'chai';
import { browser, by, element } from 'protractor';
import { Given, When, Then } from 'cucumber';
import { title } from 'process';
import { __await } from 'tslib';
import { PageOpcionesInteres } from '../pages/UserInyerface/PageOpcionesInteres';
import { exec, execSync } from "child_process";
import { runInThisContext } from "vm";
import * as cp from 'child_process';

var pageOpcionesInteres = new PageOpcionesInteres();

When('selecciono la imagen y 3 opciones de interes', async function () {
    
    await pageOpcionesInteres.HighLight(pageOpcionesInteres.UnselectAll);
    await pageOpcionesInteres.clickOnWebElement(pageOpcionesInteres.UnselectAll);

    await pageOpcionesInteres.clickOnWebElement(pageOpcionesInteres.OptionPonies);
    await pageOpcionesInteres.clickOnWebElement(pageOpcionesInteres.OptionPolo);
    await pageOpcionesInteres.clickOnWebElement(pageOpcionesInteres.OptionDough);

    //await pageOpcionesInteres.clickOnWebElement(pageOpcionesInteres.LinkUploadImage);

   // browser.sleep(5000);
    //await pageOpcionesInteres.clickOnWebElement(pageOpcionesInteres.ButtonNext);
  
});