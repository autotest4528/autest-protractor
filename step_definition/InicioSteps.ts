import { config } from "../config/config";
import { assert, Assertion } from 'chai';
import { browser } from 'protractor';
import { Given, When, Then } from 'cucumber';
import { title } from 'process';
import { PageInicio } from '../pages/UserInyerface/PageInicio';
import { __await } from 'tslib';

var pageInicio = new PageInicio();

When('elijo mis datos de ingreso', async function () {

    await pageInicio.HighLight(pageInicio.TextPassword);
    await pageInicio.clearWebElement(pageInicio.TextPassword);
    await pageInicio.sendKeysToElement(pageInicio.TextPassword, "Mypasswordis1")

    await pageInicio.HighLight(pageInicio.TextEmail);
    await pageInicio.clearWebElement(pageInicio.TextEmail);
    await pageInicio.sendKeysToElement(pageInicio.TextEmail, "emailwtest")

    await pageInicio.HighLight(pageInicio.TextDomain);
    await pageInicio.clearWebElement(pageInicio.TextDomain);
    await pageInicio.sendKeysToElement(pageInicio.TextDomain, "gmail")

    await pageInicio.clickOnWebElement(pageInicio.SelectExtention);
    await pageInicio.clickOnWebElement(pageInicio.SelectCom);
    browser.sleep(2000);
    await pageInicio.clickOnWebElement(pageInicio.ButtonTerms);
    browser.sleep(2000);
    await pageInicio.clickOnWebElement(pageInicio.ButtonNext);
  
});