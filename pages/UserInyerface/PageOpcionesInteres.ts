import { browser, element, by, Locator, protractor, $$, $} from "protractor";
import { BasePage } from "../BasePage";


export class PageOpcionesInteres extends BasePage {

    public readonly UnselectAll: Locator = by.xpath("//div[21]/div/span/label/span/span");
    public readonly ButtonDowloadImage: Locator = by.xpath("//div[2]/div/button");
    public readonly LinkUploadImage: Locator = by.xpath("//a");
    public readonly OptionPonies: Locator = by.xpath("//label/span");
    public readonly OptionPolo: Locator = by.xpath("//div[2]/div/span/label/span");
    public readonly OptionDough: Locator = by.xpath("//div[3]/div/span/label/span");
    public readonly ButtonNext: Locator = by.xpath("(//button[@name='button'])[4]");
    
}