import { browser, element, by, Locator, protractor, $$, $} from "protractor";
import { BasePage } from "../BasePage";


export class PageInicio extends BasePage {

    public readonly TextPassword: Locator = by.xpath("//input[@type='text']");
    public readonly TextEmail: Locator = by.xpath("(//input[@type='text'])[2]");
    public readonly TextDomain: Locator = by.xpath("(//input[@type='text'])[3]");
    public readonly SelectExtention: Locator = by.xpath("//div[4]/div/div/div[2]");
    public readonly SelectCom: Locator = by.xpath("//div[9]");
    public readonly ButtonTerms: Locator = by.xpath("//span/span");
    public readonly ButtonNext: Locator = by.linkText("Next");
    

}