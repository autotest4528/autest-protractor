# **Protractor UI Automation Framework for valiu**
​
## **1. Introduction**
This is the base framework based on Protractor, Cucumber and Chai for test automation.
​
The results generated in by this framework are:
​
- A JSON file that includes the test run details.
- An HTML file created from the previous JSON file for better visualization of the results.
​
### **2. Architecture and Technology Stack**
​
![Technology Stack](./screenshots/ui_stack.png?raw=true "Technology Stack")
​

## **3. Reporting**
After each run, cucumber outputs a JSON file in the directory `reports/json-reports` with the details of the run